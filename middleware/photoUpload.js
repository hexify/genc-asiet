const multer = require('multer')

const uploader = multer({
	limits: {
		fileSize: 1024 * 1024 * 5,
		files: 2,
	},
	fileFilter(req, file, cb) {
		if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/i)) {
			cb(new Error('Please upload an jpg, jpeg, png or webp file'))
		}
		req.user.picMime = file.mimetype
		cb(undefined, true)
	},
})

module.exports = uploader.fields([{ name: 'photo' }, { name: 'signature' }])
