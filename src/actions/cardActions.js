import axios from 'axios'

import { createAlert } from './alertActions'

export const generateCard = data => async dispatch => {
	try {
		const res = await axios.post('/api/generate', data, {
			'content-type': 'application/json',
		})
		let today = new Date()
		let a = document.createElement('a')
		a.href = `data:application/pdf;base64,${res.data}`
		a.download = `card${today.getFullYear()}${
			today.getMonth() + 1
		}${today.getDate()}${today.getHours()}${today.getMinutes()}.pdf`
		a.click()
		createAlert('File Generated', dispatch)
	} catch (err) {
		console.error(err)
		createAlert('Generation Failed', dispatch)
	}
}
