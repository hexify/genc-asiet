import { ALERT_ADD, ALERT_REMOVE } from './types'

export const createAlert = async (msg, dispatch) => {
	dispatch({ type: ALERT_ADD, payload: msg })
	setTimeout(() => dispatch({ type: ALERT_REMOVE, payload: msg }), 3000)
}
