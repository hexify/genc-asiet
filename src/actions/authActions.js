import axios from 'axios'

import secureRequest from '../utils/secureRequest'
import { createAlert } from './alertActions'
import { UPDATE_AUTH } from './types'

export const login = password => async dispatch => {
	try {
		const res = await axios.post('/api/auth', { password })
		localStorage.setItem('token', res.data)
		dispatch({
			type: UPDATE_AUTH,
			payload: { isAuth: true, token: res.data },
		})
		createAlert('Login Successful', dispatch)
	} catch (err) {
		console.error(err)
		dispatch({ type: UPDATE_AUTH, payload: { isAuth: false } })
		createAlert(
			err.response ? err.response.data.message : err.message,
			dispatch
		)
	}
}

export const logout = () => async dispatch => {
	localStorage.removeItem('token')
	dispatch({
		type: UPDATE_AUTH,
		payload: { isAuth: false, token: null },
	})
	createAlert('Logout Successful', dispatch)
}

export const reset = password => async dispatch => {
	try {
		const res = await secureRequest.post('/api/auth/reset', { password })
		localStorage.setItem('token', res.data)
		dispatch({
			type: UPDATE_AUTH,
			payload: { isAuth: true, token: res.data },
		})
		createAlert('Password Reset', dispatch)
	} catch (err) {
		console.error(err)
		dispatch({ type: UPDATE_AUTH, payload: { isAuth: false } })
		createAlert(
			err.response ? err.response.data.message : err.message,
			dispatch
		)
	}
}
