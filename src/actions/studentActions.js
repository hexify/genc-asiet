import secureRequest from '../utils/secureRequest'
import { createAlert } from './alertActions'
import {
	UPDATE_STUDENT,
	STUDENT_DELETED,
	STUDENT_SELECTION_CHANGED,
} from './types'

export const getStudentList = () => async dispatch => {
	try {
		const res = await secureRequest.get('/api/student')
		dispatch({ type: UPDATE_STUDENT, payload: { students: res.data } })
	} catch (err) {
		console.log(err)
		createAlert('Loading Failed', dispatch)
	}
}

export const addStudent = student => async dispatch => {
	try {
		const data = new FormData()
		for (let field in student) data.append(field, student[field])
		await secureRequest.post('/api/student', data, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		})
		createAlert('Student Added', dispatch)
	} catch (err) {
		console.log(err)
		createAlert('Failed to add student', dispatch)
	}
}

export const editStudent = student => async dispatch => {
	try {
		const data = new FormData()
		for (let field in student) data.append(field, student[field])
		await secureRequest.put('/api/student', data, {
			headers: {
				'Content-Type': 'multipart/form-data',
			},
		})
		createAlert('Student Edited', dispatch)
	} catch (err) {
		console.log(err)
		createAlert('Failed to edit student', dispatch)
	}
}

export const deleteStudent = id => async dispatch => {
	try {
		await secureRequest.delete(`/api/student/${id}`)
		dispatch({ type: STUDENT_DELETED, payload: id })
		createAlert('Student Deleted', dispatch)
	} catch (err) {
		console.log(err)
		createAlert('Failed to delete student', dispatch)
	}
}

export const changeSelection = (id, checked) => async dispatch => {
	console.log('here')
	dispatch({ type: STUDENT_SELECTION_CHANGED, payload: { id, checked } })
}
