import secureRequest from '../utils/secureRequest'
import { createAlert } from './alertActions'

export const addCourse = course => async dispatch => {
	try {
		const res = await secureRequest.post('/api/course', course, {
			'Content-Type': 'application/json',
		})
		createAlert('Course Added', dispatch)
		return res.data
	} catch (err) {
		console.log(err)
		createAlert('Failed to add course', dispatch)
		return null
	}
}

export const deleteCourse = id => async dispatch => {
	try {
		await secureRequest.delete(`/api/course/${id}`)
		createAlert('Course Deleted', dispatch)
		return true
	} catch (err) {
		console.log(err)
		createAlert('Failed to delete course', dispatch)
		return false
	}
}

export const editCourse = course => async dispatch => {
	try {
		await secureRequest.put('/api/course', course, {
			headers: {
				'Content-Type': 'application/json',
			},
		})
		createAlert('Course Edited', dispatch)
		return true
	} catch (err) {
		console.log(err)
		createAlert('Failed to edit course', dispatch)
		return true
	}
}

export const getCourses = () => async dispatch => {
	try {
		const res = await secureRequest.get('/api/course')
		return res.data
	} catch (err) {
		console.log(err)
		createAlert('Loading Failed', dispatch)
	}
}
