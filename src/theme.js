import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles({
	centerizer: {
		alignItems: 'center',
		display: 'flex',
		flexDirection: 'row',
		height: '100%',
		justifyContent: 'center',
		textAlign: 'center',
		width: '100%',
	},
	headingTop: {
		marginTop: 36,
	},
	root: {
		borderRadius: '0px',
		display: 'flex',
		flex: 1,
		textAlign: 'center',
		justifyContent: 'center',
	},
})

export default useStyles
