import axios from 'axios'

import setAuthToken from './setAuthToken'

const addToken = () => {
	if (localStorage.token) {
		setAuthToken(localStorage.token)
	}
}

const secureRequest = {
	delete: async (url, config = null) => {
		addToken()
		return config ? await axios.delete(url, config) : await axios.delete(url)
	},
	get: async (url, config = null) => {
		addToken()
		return config ? await axios.get(url, config) : await axios.get(url)
	},
	post: async (url, data = null, config = null) => {
		addToken()
		return data
			? config
				? await axios.post(url, data, config)
				: await axios.post(url, data)
			: await axios.post(url)
	},
	put: async (url, data = null, config = null) => {
		addToken()
		return data
			? config
				? await axios.put(url, data, config)
				: await axios.put(url, data)
			: await axios.put(url)
	},
}

export default secureRequest
