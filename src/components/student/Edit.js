import React, { Fragment, useEffect, useState } from 'react'
import {
	Button,
	FormControl,
	IconButton,
	InputLabel,
	makeStyles,
	Select,
	TextField,
	Typography,
} from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { connect } from 'react-redux'

import fieldsInfo from './fieldsInfo'
import { getCourses } from '../../actions/courseActions'
import { editStudent } from '../../actions/studentActions'

const Form = ({
	editStudent,
	getCourses,
	match,
	student: { students },
	history,
}) => {
	useEffect(() => {
		const func = async () => {
			let res = await getCourses()
			res = res.map(course => course.name)
			fieldsInfo.map(
				(field, index) =>
					field.name === 'course' && (fieldsInfo[index].values = res)
			)
			setFields(fieldsInfo)
		}
		func()
		// eslint-disable-next-line
	}, [])

	const localClasses = useLocalStyles()

	const initialTexts = getInitialTexts()

	const initialValues = getInitialValues()

	const inputHandler = ({ target: { files, name, value } }) => {
		let newTexts = {
			...initialTexts,
			disabled: false,
		}

		const newStudent = {
			...student,
			[name]: files ? files[0] : value,
		}

		fields.map(({ name, patt, required, text, type }) => {
			if (type === 'upload') return 0
			if (newStudent[name] === '' && required) {
				newTexts[name] = 'Cannot be empty'
				newTexts.disabled = true
				return 0
			}
			if (patt && !patt.test(newStudent[name])) {
				newTexts[name] = text
				newTexts.disabled = true
				return 0
			}
			newTexts[name] = ''
			return 0
		})

		setTexts(newTexts)
		setStudent(newStudent)
	}

	const submitHandler = event => {
		event.preventDefault()
		editStudent(student)
	}

	const currentStudent = students.filter(
		student => student._id === match.params.id
	)[0]
	if (!currentStudent) history.push('/dashboard')

	const [fields, setFields] = useState([])

	const [student, setStudent] = useState(
		currentStudent ? { ...initialValues, ...currentStudent } : initialValues
	)

	const [texts, setTexts] = useState(initialTexts)

	const backClicked = () => {
		history.push('/dashboard')
	}

	return (
		<div>
			<IconButton
				aria-label='back'
				onClick={backClicked}
				style={{ position: 'absolute', left: 12, top: 12 }}
			>
				<ArrowBackIcon />
			</IconButton>
			<Typography component='h1' variant='h4'>
				Edit Student
			</Typography>
			<form
				autoComplete='off'
				className={localClasses.form}
				onSubmit={submitHandler}
			>
				{fields.map(
					({ fullWidth, label, name, required, type, values }, index) => {
						switch (type) {
							case 'select':
								return (
									<FormControl variant='outlined' key={index}>
										<InputLabel htmlFor={name}>{label}</InputLabel>
										<Select
											inputProps={{ name, id: name }}
											label={label}
											native
											onChange={inputHandler}
											required={false}
											value={student[name]}
										>
											{values.map((value, index) => (
												<option key={index} value={value}>
													{value}
												</option>
											))}
										</Select>
									</FormControl>
								)
							case 'upload':
								return (
									<div key={index} style={{ display: 'inline-block' }}>
										<input
											accept='image/*'
											className={localClasses.uploadInput}
											id={name}
											name={name}
											onChange={inputHandler}
											required={false}
											type='file'
										/>
										<label htmlFor={name} className={localClasses.uploadLabel}>
											<Button
												className={localClasses.uploadButton}
												color='primary'
												component='div'
												size='large'
												variant='outlined'
											>
												{student[name] === '' ? label : student[name].name}
											</Button>
											<Typography
												component='p'
												color='textSecondary'
												variant='caption'
											>
												{texts[name]}
											</Typography>
										</label>
									</div>
								)
							default:
								return (
									<Fragment key={index}>
										<TextField
											autoFocus={index === 0 ? true : false}
											className={
												fullWidth
													? localClasses.textFieldBig
													: localClasses.textField
											}
											helperText={texts[name]}
											id={name}
											label={label}
											name={name}
											onChange={inputHandler}
											required={required}
											type={type ? type : 'text'}
											value={student[name]}
											variant='outlined'
											InputLabelProps={type === 'date' ? { shrink: true } : {}}
										/>
										{name.match(/^routeTo/) ? <br /> : null}
									</Fragment>
								)
						}
					}
				)}
				<div>
					<Button
						color='primary'
						disabled={texts.disabled}
						type='submit'
						variant='contained'
					>
						Submit
					</Button>
				</div>
			</form>
		</div>
	)
}

const getInitialTexts = () => {
	let initialTexts = {}
	fieldsInfo.map(({ name }) => (initialTexts[name] = ''))
	initialTexts.disabled = true
	return initialTexts
}

const getInitialValues = () => {
	let initialValues = {}
	fieldsInfo.map(
		({ name, type, values }) =>
			(initialValues[name] = type === 'select' ? values[0] : '')
	)
	return initialValues
}

const useLocalStyles = makeStyles(theme => ({
	form: {
		padding: theme.spacing(2),
		'& > *': {
			margin: theme.spacing(2),
			minWidth: 360,
		},
	},
	textField: {
		maxWidth: 360,
	},
	textFieldBig: {
		maxWidth: '84vw',
		width: '100%',
	},
	uploadInput: {
		display: 'none',
	},
	uploadLabel: {
		display: 'inline-flex',
		flexDirection: 'column',
		maxWidth: 360,
		padding: theme.spacing(1) + 'px 0px',
		width: '100%',
	},
}))

const mapStateToProps = state => ({
	student: state.student,
})

export default connect(mapStateToProps, { editStudent, getCourses })(Form)
