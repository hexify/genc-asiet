const fields = [
	{
		label: 'Name',
		name: 'name',
		patt: /^[a-zA-Z. ]*$/,
		required: true,
		text: 'Invalid Name',
	},
	{
		fullWidth: true,
		label: 'Address',
		name: 'address',
		required: true,
	},
	{
		label: 'Date of Birth',
		name: 'dob',
		required: true,
		type: 'date',
	},
	{
		label: 'Course of Study',
		name: 'course',
		required: true,
		type: 'select',
		values: [],
	},
	{
		label: 'Duration of Course',
		name: 'duration',
		required: true,
		type: 'select',
		values: [1, 2, 3, 4],
	},
	{
		label: 'Date of Issue',
		name: 'issueDate',
		required: true,
		type: 'date',
	},
	{
		label: 'Date of Expiry',
		name: 'expiryDate',
		required: true,
		type: 'date',
	},
	{
		label: 'Phone Number',
		name: 'phone',
		patt: /^(\+91|0)?[0-9]{10}$/,
		required: true,
		text: 'Invalid phone number',
	},
	{
		label: 'Mobile Number',
		name: 'mobile',
		patt: /^(\+91|0)?[0-9]{10}$/,
		required: true,
		text: 'Invalid mobile number',
	},
	{
		label: 'Photo',
		name: 'photo',
		required: true,
		type: 'upload',
	},
	{
		label: 'Signature',
		name: 'signature',
		required: true,
		type: 'upload',
	},
	{
		label: 'Route 1 From',
		name: 'routeFrom1',
		required: true,
	},
	{
		label: 'Route 1 To',
		name: 'routeTo1',
		required: true,
	},
	{
		label: 'Route 2 From',
		name: 'routeFrom2',
	},
	{
		label: 'Route 2 To',
		name: 'routeTo2',
	},
	{
		label: 'Route 3 From',
		name: 'routeFrom3',
	},
	{
		label: 'Route 3 To',
		name: 'routeTo3',
	},
	{
		label: 'Route 4 From',
		name: 'routeFrom4',
	},
	{
		label: 'Route 4 To',
		name: 'routeTo4',
	},
]

export default fields
