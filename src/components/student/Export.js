import React, { useEffect } from 'react'
import { IconButton, Typography } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { Parser } from 'json2csv'
import { connect } from 'react-redux'

import useStyles from '../../theme'

const Export = ({ history, student: { students } }) => {
	useEffect(() => {
		try {
			let fields = []
			for (let field in students[0]) fields.push(field)
			const parser = new Parser({ fields })
			const csv = parser.parse(students)
			let today = new Date()
			let a = document.createElement('a')
			a.href = `data:application/csv;${csv}`
			a.download = `export-${today.getFullYear()}${
				today.getMonth() + 1
			}${today.getDate()}${today.getHours()}${today.getMinutes()}.csv`
			a.click()
			backClicked()
		} catch (err) {
			console.error(err)
		}
		// eslint-disable-next-line
	}, [])

	const backClicked = () => {
		history.push('/dashboard')
	}

	const Classes = useStyles()

	return (
		<>
			<IconButton
				aria-label='back'
				onClick={backClicked}
				style={{ position: 'absolute', left: 12, top: 12 }}
			>
				<ArrowBack />
			</IconButton>
			<div className={Classes.centerizer}>
				<Typography component='p' variant='h4'>
					Loading...
				</Typography>
			</div>
		</>
	)
}

const mapStateToProps = state => ({
	student: state.student,
})

export default connect(mapStateToProps)(Export)
