import React from 'react'
import { Button, Fade, Typography, Paper, Checkbox } from '@material-ui/core'
import {
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	IconButton,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import { connect } from 'react-redux'

import { deleteStudent, changeSelection } from '../../actions/studentActions'

const ListItem = ({
	history,
	deleteStudent,
	changeSelection,
	checked,
	_id,
	name,
	cardGenerated,
	course,
}) => {
	const [open, setOpen] = React.useState(false)
	const [zoomed, setZoomed] = React.useState(true)

	const editClicked = () => {
		history.push('/student/edit/' + _id)
	}

	const removeClicked = () => {
		setOpen(true)
	}

	const handleClose = () => {
		setOpen(false)
	}

	const handleCheckBoxChange = event => {
		changeSelection(_id, event.target.checked)
	}

	const removeConfirmed = async () => {
		setOpen(false)
		setZoomed(false)
		setTimeout(() => {
			deleteStudent(_id)
		}, 500)
	}

	return (
		<>
			<Fade in={zoomed}>
				<Paper elevation={3} style={{ margin: 10, padding: 15 }}>
					<div style={{ display: 'flex', alignItems: 'center' }}>
						{cardGenerated ? (
							<></>
						) : (
							<Checkbox
								checked={checked}
								onChange={handleCheckBoxChange}
								style={{ color: '#1e88e5', marginRight: 20 }}
							/>
						)}
						<Typography
							component='p'
							variant='h6'
							align='left'
							style={{ flex: 1 }}
						>
							{name}
						</Typography>
						<Typography
							component='p'
							variant='h6'
							style={{ flex: 2 }}
							align='left'
						>
							{' '}
							{course}
						</Typography>
						<IconButton aria-label='edit' onClick={editClicked} style={{color: '#1e88e5'}}>
							<EditIcon />
						</IconButton>
						<IconButton aria-label='remove' onClick={removeClicked} style={{color: 'red'}}>
							<DeleteIcon />
						</IconButton>
					</div>
				</Paper>
			</Fade>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby='alert-dialog-title'
				aria-describedby='alert-dialog-description'
			>
				<DialogTitle id='alert-dialog-title'>Deletion Confirmation</DialogTitle>
				<DialogContent>
					<DialogContentText id='alert-dialog-description'>
						{'Are you sure you want to delete ' + name + ' ?'}
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color='primary' autoFocus>
						Cancel
					</Button>
					<Button onClick={removeConfirmed} color='secondary'>
						Delete
					</Button>
				</DialogActions>
			</Dialog>
		</>
	)
}

export default connect(null, { deleteStudent, changeSelection })(ListItem)
