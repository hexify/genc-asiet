import React, { Component } from 'react'
import { Typography, TextField, InputAdornment } from '@material-ui/core'
import SearchRoundedIcon from '@material-ui/icons/SearchRounded'
import { connect } from 'react-redux'

import ListItem from './ListItem'
import { getStudentList } from '../../actions/studentActions'

class List extends Component {
	constructor(props) {
		super(props)
		this.state = {
			pendingStudents: [],
			completedStudents: [],
			searchString: '',
			date: '',
		}
	}

	componentDidMount() {
		this.props.getStudentList()
	}

	componentDidUpdate(prevProps) {
		const oldStudents = prevProps.student.students
		const newStudents = this.props.student.students
		if (oldStudents !== newStudents) this.sortStudents(newStudents)
	}

	sortStudents(newStudents) {
		const completedStudents = []
		const pendingStudents = []

		newStudents.forEach(student => {
			;(student.cardGenerated ? completedStudents : pendingStudents).push(
				student
			)
		})

		this.setState({
			completedStudents,
			pendingStudents,
		})
	}

	render() {
		const {
			completedStudents,
			pendingStudents,
			searchString,
			date,
		} = this.state
		return (
			<>
				<div
					style={{
						display: 'flex',
						flex: 1,
						flexDirection: 'column',
					}}
				>
					<div style={{ alignSelf: 'center' }}>
						<TextField
							id='input-with-icon-textfield'
							InputProps={{
								startAdornment: (
									<InputAdornment position='start'>
										<SearchRoundedIcon />
									</InputAdornment>
								),
							}}
							placeholder='Search'
							defaultValue={searchString}
							onChange={({ target: { value } }) =>
								this.setState({ searchString: value.toLowerCase() })
							}
						/>
						<TextField
							defaultValue={date}
							style={{ margin: '0px 12px' }}
							type='date'
							onChange={({ target: { value } }) =>
								this.setState({ date: value })
							}
						/>
					</div>
					{pendingStudents.length === 0 ? (
						<div
							style={{
								justifyContent: 'center',
								flex: 1,
								flexDirection: 'column',
								display: 'flex',
							}}
						>
							<Typography
								component='p'
								variant='h4'
								style={{ margin: '24px 0px' }}
							>
								No Pending Cards
							</Typography>
						</div>
					) : (
						<>
							<Typography
								component='p'
								variant='h4'
								align='left'
								style={{ marginLeft: 20, marginTop: 20 }}
							>
								Pending
							</Typography>
							{pendingStudents
								.filter(student => {
									if (date) {
										const cDate = new Date(date)
										const sDate = new Date(student.created)
										return (
											student.name.toLowerCase().includes(searchString) &&
											cDate.getFullYear() === sDate.getFullYear() &&
											cDate.getMonth() === sDate.getMonth() &&
											cDate.getDate() === sDate.getDate()
										)
									}
									return student.name.toLowerCase().includes(searchString)
								})
								.map(student => (
									<ListItem
										key={student._id}
										history={this.props.history}
										{...student}
									/>
								))}
						</>
					)}

					{completedStudents.length > 0 && (
						<>
							<Typography
								component='p'
								variant='h4'
								align='left'
								style={{ marginLeft: 20, marginTop: 20 }}
							>
								Completed
							</Typography>
							{completedStudents
								.filter(student =>
									student.name.toLowerCase().includes(searchString)
								)
								.map(student => (
									<ListItem
										key={student._id}
										history={this.props.history}
										{...student}
									/>
								))}
						</>
					)}
				</div>
			</>
		)
	}
}

const mapStateToProps = state => ({
	student: state.student,
})

export default connect(mapStateToProps, { getStudentList })(List)
