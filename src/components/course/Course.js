import React, { useState } from 'react'
import { Button, makeStyles, TextField } from '@material-ui/core'
import { Delete, Done, Edit } from '@material-ui/icons'

const Course = ({ _id, action, deleteAction, mode, name }) => {
	const deleteClicked = () => {
		deleteAction(_id)
	}

	const inputHandler = ({ target: { value } }) => setValue(value)

	const LocalClasses = useLocalStyles()

	const toggleClicked = () => {
		if (Clicked) action(Value, name)
		setClicked(!Clicked)
	}

	const [Clicked, setClicked] = useState(false)

	const [Value, setValue] = useState(mode === 'edit' ? name : '')

	return (
		<div className={LocalClasses.root}>
			{Clicked ? (
				<TextField
					autoFocus
					className={LocalClasses.button}
					onChange={inputHandler}
					value={Value}
					variant='outlined'
				/>
			) : (
				<Button
					className={LocalClasses.button}
					onClick={toggleClicked}
					variant='outlined'
				>
					{mode === 'edit' ? name : 'Add Course'}
				</Button>
			)}
			{Clicked ? (
				<Button
					className={LocalClasses.button}
					onClick={toggleClicked}
					variant='contained'
				>
					<Done />
				</Button>
			) : (
				<>
					{mode === 'edit' && (
						<>
							<Button
								className={LocalClasses.button}
								onClick={toggleClicked}
								variant='contained'
							>
								<Edit />
							</Button>
							<Button
								className={LocalClasses.button}
								onClick={deleteClicked}
								variant='contained'
							>
								<Delete />
							</Button>
						</>
					)}
				</>
			)}
		</div>
	)
}

const useLocalStyles = makeStyles(theme => ({
	button: {
		margin: theme.spacing(1),
		textTransform: 'none',
	},
	root: {
		display: 'flex',
		flexDirection: 'row',
	},
}))

export default Course
