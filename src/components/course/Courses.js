import React, { useEffect, useState } from 'react'
import { IconButton, makeStyles } from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { connect } from 'react-redux'

import Course from './Course'
import {
	addCourse,
	deleteCourse,
	editCourse,
	getCourses,
} from '../../actions/courseActions'

const Courses = ({
	addCourse,
	deleteCourse,
	editCourse,
	getCourses,
	history,
}) => {
	useEffect(() => {
		const func = async () => {
			const res = await getCourses()
			if (res) setCourses(res)
		}
		func()
		// eslint-disable-next-line
	}, [])

	const addCourseHandler = async name => {
		if (name === '') return
		const res = await addCourse({ name })
		if (res) setCourses([{ ...res, name }, ...Courses])
	}

	const deleteCourseHandler = async id => {
		const res = await deleteCourse(id)
		if (res) setCourses(Courses.filter(course => course._id !== id))
	}

	const editCourseHandler = async (name, old) => {
		if (name === old) return

		let [courseToEdit] = Courses.filter(course => course.name === old)

		const res = await editCourse({ ...courseToEdit, name })
		if (!res) return

		let newCourses = Courses.map(course => {
			if (course.name === old) return { ...course, name }
			return course
		})
		setCourses(newCourses)
	}

	const backClicked = () => {
		history.push('/dashboard')
	}

	const LocalClasses = useLocalStyles()

	const [Courses, setCourses] = useState([])

	return (
		<div className={LocalClasses.root}>
			<IconButton
				aria-label='back'
				onClick={backClicked}
				style={{ position: 'absolute', left: 12, top: 12 }}
			>
				<ArrowBack />
			</IconButton>
			<Course action={addCourseHandler} mode='add' />
			{Courses.map((course, key) => (
				<Course
					action={editCourseHandler}
					deleteAction={deleteCourseHandler}
					key={key}
					mode='edit'
					{...course}
				/>
			))}
		</div>
	)
}

const useLocalStyles = makeStyles(theme => ({
	root: {
		alignItems: 'center',
		display: 'flex',
		flexDirection: 'column',
		margin: theme.spacing(3),
		marginTop: theme.spacing(6),
	},
}))

export default connect(null, {
	addCourse,
	deleteCourse,
	editCourse,
	getCourses,
})(Courses)
