import React, { useEffect } from 'react'
import { makeStyles, Paper, Typography } from '@material-ui/core'
import { grey } from '@material-ui/core/colors'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

const Alert = ({ alerts, history }) => {
	useEffect(() => {
		alerts.map(alert => {
			switch (alert) {
				case 'File Generated':
				case 'Generation Failed':
				case 'Login Successful':
				case 'Password Reset':
				case 'Student Added':
				case 'Student Edited':
				case 'Student Deleted':
					return history.push('/dashboard')
				case 'Logout Successful':
				case 'Token is invalid':
				case 'Token required':
					return history.push('/')
				default:
					return 0
			}
		})
		// eslint-disable-next-line
	}, [alerts])

	const localClasses = useLocalStyles()

	return (
		<div className={localClasses.alerts}>
			{alerts.map((alert, index) => (
				<Paper className={localClasses.alert} elevation={3} key={index}>
					<Typography variant='body1'>{alert}</Typography>
				</Paper>
			))}
		</div>
	)
}

const mapStatesToProps = state => ({
	alerts: state.alerts,
})

const useLocalStyles = makeStyles(theme => ({
	alert: {
		color: 'white',
		backgroundColor: grey[800],
		margin: theme.spacing(1) + 'px auto',
		maxWidth: '240px',
		padding: theme.spacing(1),
		width: '80vw',
	},
	alerts: {
		bottom: 0,
		left: 0,
		position: 'fixed',
		right: 0,
	},
}))

export default withRouter(connect(mapStatesToProps)(Alert))
