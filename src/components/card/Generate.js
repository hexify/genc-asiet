import React, { useEffect } from 'react'
import { Typography } from '@material-ui/core'
import { connect } from 'react-redux'

import useStyles from '../../theme'
import { generateCard } from '../../actions/cardActions'

const Generate = ({ generateCard, student: { students } }) => {
	useEffect(() => {
		let selectedStudents = students.filter(student => student.checked)
		if (selectedStudents.length === 0)
			selectedStudents = students.filter(
				student => student.cardGenerated !== true
			)
		selectedStudents = selectedStudents.map(student => student._id)
		generateCard(selectedStudents)
	})

	const classes = useStyles()

	return (
		<div className={classes.centerizer}>
			<Typography component='p' variant='h4'>
				Loading...
			</Typography>
		</div>
	)
}

const mapStateToProps = state => ({
	student: state.student,
})

export default connect(mapStateToProps, { generateCard })(Generate)
