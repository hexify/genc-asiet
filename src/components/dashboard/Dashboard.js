import React from 'react'
import { Button, makeStyles } from '@material-ui/core'
import { Link } from 'react-router-dom'

import List from '../student/List'

const Dashboard = ({ history }) => {
	const localClasses = useLocalStyles()
	return (
		<div className={localClasses.root}>
			<div>
				<Link className={localClasses.button} to='/student/add'>
					<Button variant='outlined'>Add Student</Button>
				</Link>
				<Link className={localClasses.button} to='/generate'>
					<Button variant='outlined' style={{ marginLeft: 5, marginRight: 5 }}>
						Generate Cards
					</Button>
				</Link>
				<Link className={localClasses.button} to='/courses'>
					<Button variant='outlined'>Courses</Button>
				</Link>
				<Link className={localClasses.button} to='/reset'>
					<Button variant='outlined'>Reset Password</Button>
				</Link>
				<Link className={localClasses.button} to='/export'>
					<Button variant='outlined'>Export</Button>
				</Link>
				<Link className={localClasses.button} to='/logout'>
					<Button variant='outlined'>Logout</Button>
				</Link>
			</div>
			<List history={history} />
		</div>
	)
}

const useLocalStyles = makeStyles(theme => ({
	button: { display: 'inline-block', margin: theme.spacing(3) },
	root: { display: 'flex', flexDirection: 'column', width: '100%' },
}))

export default Dashboard
