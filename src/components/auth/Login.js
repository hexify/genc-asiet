import React, { useState } from 'react'
import {
	Button,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import { connect } from 'react-redux'

import { login } from '../../actions/authActions'
import useStyles from '../../theme'

const Login = ({ login }) => {
	const classes = useStyles()

	const inputHandler = ({ target: { value } }) => {
		const patt = /^[a-zA-Z0-9!@#$%^&*]+$/
		const result = patt.test(value)
		setText({
			disabled: !result,
			password: result ? '' : 'Invalid Password',
		})
		setPassword(value)
	}

	const localClasses = useLocalStyles()

	const submitHandler = event => {
		event.preventDefault()
		login(password)
	}

	const [clicked, setClicked] = useState(false)

	const [text, setText] = useState({ disabled: true, password: '' })

	const [password, setPassword] = useState('')

	if (clicked)
		return (
			<div className={classes.centerizer}>
				<form onSubmit={submitHandler}>
					<Paper className={localClasses.paper}>
						<TextField
							autoFocus
							helperText={text.password}
							label='Password'
							name='password'
							onChange={inputHandler}
							type='password'
							value={password}
							variant='outlined'
						/>
						<Button
							color='primary'
							disabled={text.disabled}
							type='submit'
							variant='contained'
						>
							Login
						</Button>
					</Paper>
				</form>
			</div>
		)
	return (
		<div className={classes.centerizer} style={{ flexDirection: 'column' }}>
			<img src='icon.png' alt='Gen C A S I E T Icon' style={{ width: 120 }} />
			<Typography
				component='h1'
				style={{ margin: '12px 0px 48px 0px' }}
				variant='h2'
			>
				GenC ASIET
			</Typography>
			<Button
				color='primary'
				onClick={() => setClicked(true)}
				variant='contained'
			>
				Login
			</Button>
		</div>
	)
}

const useLocalStyles = makeStyles(theme => ({
	paper: {
		display: 'flex',
		flexDirection: 'column',
		padding: theme.spacing(2),
		'& > *': {
			margin: theme.spacing(2),
			width: 360,
		},
	},
}))

export default connect(null, { login })(Login)
