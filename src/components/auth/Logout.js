import React, { useEffect } from 'react'
import { Typography } from '@material-ui/core'
import { connect } from 'react-redux'

import useStyles from '../../theme'
import { logout } from '../../actions/authActions'

const Logout = ({ logout }) => {
	useEffect(() => {
		logout()
	})

	const classes = useStyles()

	return (
		<div className={classes.centerizer}>
			<Typography component='p' variant='h4'>
				Loading...
			</Typography>
		</div>
	)
}

export default connect(null, { logout })(Logout)
