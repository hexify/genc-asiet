import React, { useState } from 'react'
import {
	Button,
	IconButton,
	makeStyles,
	Paper,
	TextField,
	Typography,
} from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { connect } from 'react-redux'

import { reset } from '../../actions/authActions'
import useStyles from '../../theme'

const Reset = ({ reset, history }) => {
	const classes = useStyles()

	const inputHandler = ({ target: { name, value } }) => {
		const patt = /^[a-zA-Z0-9!@#$%^&*]+$/
		const result = patt.test(value)
		setText({
			...text,
			disabled: !result,
			[name]: result ? '' : 'Invalid Password',
		})
		setPassword({
			...password,
			[name]: value,
		})
	}

	const localClasses = useLocalStyles()

	const submitHandler = event => {
		event.preventDefault()
		reset(password.password)
	}

	const [text, setText] = useState({
		disabled: true,
		password: '',
		passwordConfirm: '',
	})

	const [password, setPassword] = useState({
		password: '',
		passwordConfirm: '',
	})

	const backClicked = () => {
		history.push('/dashboard')
	}

	return (
		<div className={classes.centerizer}>
			<IconButton
				aria-label='back'
				onClick={backClicked}
				style={{ position: 'absolute', left: 12, top: 12 }}
			>
				<ArrowBackIcon />
			</IconButton>
			<form onSubmit={submitHandler}>
				<Paper className={localClasses.paper}>
					<Typography variant='h4'>Reset Password</Typography>
					<TextField
						autoFocus
						helperText={text.password}
						label='Password'
						name='password'
						onChange={inputHandler}
						type='password'
						value={password.password}
						variant='outlined'
					/>
					<TextField
						helperText={text.passwordConfirm}
						label='Confirm Password'
						name='passwordConfirm'
						onChange={inputHandler}
						type='password'
						value={password.passwordConfirm}
						variant='outlined'
					/>
					<Button
						color='primary'
						disabled={
							text.disabled || password.password !== password.passwordConfirm
						}
						type='submit'
						variant='contained'
					>
						Reset
					</Button>
				</Paper>
			</form>
		</div>
	)
}

const useLocalStyles = makeStyles(theme => ({
	paper: {
		display: 'flex',
		flexDirection: 'column',
		padding: theme.spacing(2),
		'& > *': {
			margin: theme.spacing(2),
			width: 360,
		},
	},
}))

export default connect(null, { reset })(Reset)
