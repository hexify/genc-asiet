import 'typeface-roboto'
import React from 'react'
import { Paper } from '@material-ui/core'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import './App.css'
import Add from './components/student/Add'
import Alerts from './components/alert/Alerts'
import Courses from './components/course/Courses'
import Dashboard from './components/dashboard/Dashboard'
import Edit from './components/student/Edit'
import Export from './components/student/Export'
import Generate from './components/card/Generate'
import Login from './components/auth/Login'
import Logout from './components/auth/Logout'
import Reset from './components/auth/Reset'
import store from './store'
import useStyles from './theme'

const App = () => {
	const classes = useStyles()

	return (
		<Provider store={store}>
			<Paper className={classes.root} elevation={0}>
				<Router>
					<Switch>
						<Route exact path='/' component={Login} />
						<Route exact path='/courses' component={Courses} />
						<Route exact path='/dashboard' component={Dashboard} />
						<Route exact path='/export' component={Export} />
						<Route exact path='/generate' component={Generate} />
						<Route exact path='/logout' component={Logout} />
						<Route exact path='/reset' component={Reset} />
						<Route exact path='/student/add' component={Add} />
						<Route exact path='/student/edit/:id' component={Edit} />
					</Switch>
					<Alerts />
				</Router>
			</Paper>
		</Provider>
	)
}

export default App
