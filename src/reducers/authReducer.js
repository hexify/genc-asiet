import { UPDATE_AUTH } from '../actions/types'

const initialState = {
	isAuth: false,
	token: null,
}

export default (state = initialState, action) =>
	action.type === UPDATE_AUTH ? { ...state, ...action.payload } : state
