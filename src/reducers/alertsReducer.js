import { ALERT_ADD, ALERT_REMOVE } from '../actions/types'

const initialState = []

export default (state = initialState, action) => {
	switch (action.type) {
		case ALERT_ADD:
			return [...state, action.payload]
		case ALERT_REMOVE:
			let copy = state
			for (let alert in copy)
				if (copy[alert] === action.payload) copy.splice(alert, 1)
			return [...copy]
		default:
			return state
	}
}
