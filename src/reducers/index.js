import { combineReducers } from 'redux'

import alertsReducer from './alertsReducer'
import authReducer from './authReducer'
import studentReducer from './studentReducer'

export default combineReducers({
	alerts: alertsReducer,
	auth: authReducer,
	student: studentReducer,
})
