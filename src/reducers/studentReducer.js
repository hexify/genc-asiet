import { UPDATE_STUDENT, STUDENT_DELETED, STUDENT_SELECTION_CHANGED } from '../actions/types'

const initialState = {
	students: []
}

// export default (state = initialState, action) =>
// 	action.type === UPDATE_STUDENT ? { ...state, ...action.payload } : state

export default (state = initialState, action) => {
	let students
	switch (action.type) {
		case UPDATE_STUDENT:
			return {
				...state,
				...action.payload
			}
		case STUDENT_DELETED:
			students = [...state.students]
			students = students.filter(student => student._id !== action.payload)
			return {
				...state,
				students
			}
		case STUDENT_SELECTION_CHANGED:
			students = [...state.students]
			students.some(student => {
				if (student._id === action.payload.id) {
					student.checked = action.payload.checked
					return true
				}
				return false
			})
			return {
				...state,
				students
			}
		default:
			return state
	}
}