const config = require('config')
const express = require('express')
const PouchDB = require('pouchdb')

const auth = require('../middleware/auth')
const photoUpload = require('../middleware/photoUpload')

const dateTime = new Date()
const router = express.Router()

PouchDB.plugin(require('pouchdb-find'))
const db = new PouchDB(config.get('studentsDB'))
db.info()

router.get('/', auth, async (req, res) => {
	try {
		let students = await db.find({
			selector: { name: { $exists: true } },
		})
		students = students.docs
		students.map(student => {
			delete student.photo
			delete student.photoMime
			delete student.signature
			delete student.signatureMime
			return student
		})
		res.json(students)
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.post('/', [auth, photoUpload], async (req, res) => {
	try {
		let student = {
			...req.body,
			photo: req.files.photo[0].buffer.toString('base64'),
			photoMime: req.files.photo[0].mimetype,
			signature: req.files.signature[0].buffer.toString('base64'),
			signatureMime: req.files.signature[0].mimetype,
			created: dateTime,
			modified: dateTime,
		}
		await db.post(student)
		res.send('Student Added')
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.put('/', [auth, photoUpload], async (req, res) => {
	try {
		const data = { ...req.body }
		let student = await db.get(data._id)

		if (!student) return res.status(401).send('Student not found!')

		delete data._id
		delete data.photo
		delete data.signature
		Object.keys(data).some(key => {
			student[key] = data[key]
		})

		if (req.files.photo) {
			student.photo = req.files.photo[0].buffer.toString('base64')
			student.photoMime = req.files.photo[0].mimetype
		}
		if (req.files.signature) {
			student.signature = req.files.signature[0].buffer.toString('base64')
			student.signatureMime = req.files.signature[0].mimetype
		}

		student.modified = dateTime

		await db.put(student)
		res.send('Student Edited')
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.delete('/:id', auth, async (req, res) => {
	try {
		student = await db.get(req.params.id)
		await db.remove(student)
		res.send('Student Deleted')
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

module.exports = router
