const express = require('express')
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const PouchDB = require('pouchdb')

const auth = require('../middleware/auth')

const router = express.Router()
const db = new PouchDB(config.get('passwordDB'))
db.info()

router.post(
	'/',
	[check('password', 'Invalid password').matches(/^[a-zA-Z0-9!@#$%^&*]+$/)],
	async (req, res) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			let message = ''
			errors.array().map(({ msg }) => (message += msg + '\n'))
			return res.status(400).json({ message })
		}

		const { password: passwordInput } = req.body

		try {
			let password = await db.get('admin')
			password = password.password

			const isMatch = await bcrypt.compare(passwordInput, password)
			if (!isMatch) {
				return res.status(400).json({ message: 'Wrong Password' })
			}

			jwt.sign(
				{ user: 'admin' },
				config.get('jwtSecret'),
				{ expiresIn: 86400 },
				(err, token) => {
					if (err) throw err
					res.send(token)
				}
			)
		} catch (err) {
			console.error(err)
			res.status(500).send(err.message)
		}
	}
)

router.post(
	'/reset',
	[
		auth,
		[check('password', 'Invalid password').matches(/^[a-zA-Z0-9!@#$%^&*]+$/)],
	],
	async (req, res) => {
		const errors = validationResult(req)
		if (!errors.isEmpty()) {
			let message = ''
			errors.array().map(({ msg }) => (message += msg + '\n'))
			return res.status(400).json({ message })
		}

		let { password } = req.body

		try {
			const salt = await bcrypt.genSalt(10)
			password = await bcrypt.hash(password, salt)
			let doc = await db.get('admin')
			await db.put({ ...doc, password })

			jwt.sign(
				{ user: 'admin' },
				config.get('jwtSecret'),
				{ expiresIn: 86400 },
				(err, token) => {
					if (err) throw err
					res.send(token)
				}
			)
		} catch (err) {
			console.error(err)
			res.status(500).send(err)
		}
	}
)

module.exports = router
