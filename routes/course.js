const config = require('config')
const express = require('express')
const PouchDB = require('pouchdb')

const auth = require('../middleware/auth')
const router = express.Router()

PouchDB.plugin(require('pouchdb-find'))
const db = new PouchDB(config.get('coursesDB'))
db.info()

router.delete('/:id', auth, async (req, res) => {
	try {
		course = await db.get(req.params.id)
		await db.remove(course)
		res.send('Course Deleted')
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.get('/', auth, async (req, res) => {
	try {
		let courses = await db.find({
			selector: { name: { $exists: true } },
		})
		res.json(courses.docs)
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.post('/', auth, async (req, res) => {
	try {
		const doc = await db.post({ ...req.body })
		res.send(doc)
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

router.put('/', auth, async (req, res) => {
	try {
		const data = { ...req.body }
		let course = await db.get(data._id)

		if (!course) return res.status(401).send('Course not found')

		course = { ...course, ...data }

		await db.put(course)
		res.send('Course Updated')
	} catch (err) {
		console.error(err)
		res.status(500).send(err.message)
	}
})

module.exports = router
