const concat = require('concat-stream')
const config = require('config')
const express = require('express')
const fs = require('fs')
const path = require('path')
const PouchDB = require('pouchdb')
const PDFDocument = require('pdfkit')
const SVGtoPDF = require('svg-to-pdfkit')

const auth = require('../middleware/auth')

const router = express.Router()

PouchDB.plugin(require('pouchdb-find'))
const Id = new PouchDB(config.get('idDB'))
Id.info()
const Student = new PouchDB(config.get('studentsDB'))
Id.info()

router.post('/', auth, async (req, res) => {
	try {
		if (req.body.length === 0) {
			return res.status(500).send('Empty List')
		}

		let cardIdDoc = await Id.get('cardid')
		let mainId = cardIdDoc._id
		cardId = cardIdDoc.no

		const placeHolders = {
			cardID: 'gcid',
			photo: 'gcphoto',
			signature: 'gcsignature',
			name: 'gcname',
			age: 'gcyear',
			dob: 'gcdob',
			course: 'gccourse',
			duration: 'gcduration',
			routeFrom1: 'gcroutefrom0',
			routeTo1: 'gcrouteto0',
			routeFrom2: 'gcroutefrom1',
			routeTo2: 'gcrouteto1',
			routeFrom3: 'gcroutefrom2',
			routeTo3: 'gcrouteto2',
			routeFrom4: 'gcroutefrom3',
			routeTo4: 'gcrouteto3',
			issueDate: 'gcissue',
			expiryDate: 'gcexpiry',
			mobile: 'mobile',
			phone: 'phone',
			address: 'address',
		}

		let studentIds = req.body
		let studentInfoList = await Student.find({
			selector: { _id: { $in: studentIds } },
		})
		studentInfoList = studentInfoList.docs
		let copyList = studentInfoList

		studentInfoList = studentInfoList.map((student, index) => {
			let dob = new Date(student.dob)
			let issueDate = new Date(student.issueDate)
			let expiryDate = new Date(student.expiryDate)
			let diff = new Date(Date.now() - dob.getTime())
			copyList[index].cardID = student.cardID =
				'63-20-1' + ('000' + (++cardId % 1000)).substr(-3)
			student.age = Math.abs(diff.getUTCFullYear() - 1970).toString()
			student.dob = `${dob.getDate()}-${
				dob.getMonth() + 1
			}-${dob.getFullYear()}`
			student.issueDate = `${issueDate.getDate()}-${
				issueDate.getMonth() + 1
			}-${issueDate.getFullYear()}`
			student.expiryDate = `${expiryDate.getDate()}-${
				expiryDate.getMonth() + 1
			}-${expiryDate.getFullYear()}`
			student.photo = `data:${student.photoMime};base64,${student.photo}`
			student.signature = `data:${student.signatureMime};base64,${student.signature}`
			return student
		})

		let doc = new PDFDocument({ size: [595, 842] })
		let j = 0
		let len = studentInfoList.length
		if (parseInt(len / 5) > 0) {
			for (let i = 0; i < parseInt(len / 5); i++) {
				let cardSVGFile = path.resolve(__dirname, '../card/card5.svg')
				let svg = fs.readFileSync(cardSVGFile).toString()
				do {
					for (info in placeHolders) {
						svg = svg.replace(
							placeHolders[info] + (j % 5),
							studentInfoList[j][info]
						)
					}
					++j
				} while (j % 5 !== 0)

				SVGtoPDF(doc, svg, 0, 0)
			}
			doc.addPage()
		}
		let cardSVGFile = path.resolve(__dirname, `../card/card${len % 5}.svg`)
		let svg = fs.readFileSync(cardSVGFile).toString()
		for (let i = 0; i < len % 5; i++, j++) {
			for (info in placeHolders) {
				svg = svg.replace(
					placeHolders[info] + (j % 5),
					studentInfoList[j][info]
				)
			}
		}
		SVGtoPDF(doc, svg, 0, 0)
		doc.end()
		copyList.map(async student => {
			student.cardGenerated = true
			await Student.put(student)
		})
		cardIdDoc.no = cardId
		await Id.put(cardIdDoc)
		doc.pipe(concat(buffer => res.send(buffer.toString('base64'))))
	} catch (err) {
		console.error(err)
		return res.status(500).send('Server Error')
	}
})

module.exports = router
