# genc-asiet

Private Bus Concession Card generator for Adi Shankara Institute of Engineering and Technology

## How to Run

1. Install NodeJS and NPM.
2. Inatall dependencies with the command

   ```bash
   npm install
   ```

3. Build the frontend with the command

   ```bash
   npm run build
   ```

4. Build the app with the command

   ```bash
   npm run dist
   ```

5. Executable fill will be found in the `dist` folder.

## APIs

### Auth

- POST /api/auth/ : To authenticate user.
- POST /api/auth/reset : To reset password.

### Generate

- POST /api/generate : Generates cards for specified students.

### Student

- GET / : Returns all students without images.
- POST / : Save student details.
- PUT / : Edit student details.
