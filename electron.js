const { app, BrowserWindow } = require('electron')

const server = require('./express')

let mainWindow
const PORT = process.env.PORT || 5000

const createWindow = () => {
	mainWindow = new BrowserWindow({
		backgroundThrottling: false,
		center: true,
		titleBarStyle: 'hidden',
	})
	mainWindow.loadURL(`http://localhost:${PORT}`)
	mainWindow.maximize()
	mainWindow.on('closed', () => (mainWindow = null))
}

const start = () => {
	server.listen(PORT)
	createWindow()
}

app.on('ready', start)

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', () => {
	if (mainWindow === null) {
		start()
	}
})
