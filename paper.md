---
title: 'GenC: Student Concession Card Generator'
tags:
  - private bus concession card
  - motor vehicles department
  - kerala
  - cross-platform
  - desktop application
authors:
  - name: Jagannath Bhat
    affiliation: 1
  - name: Abhijith P
    affiliation: 1
  - name: Jose J Maliyakal
    affiliation: 1
  - name: Manish T I
    affiliation: 2
affiliations:
  - name: UG Student, Dept of CSE, Adi Shankara Institute of Engineering & Technology, Kalady
    index: 1
  - name: Professor & Dean, Dept of CSE, Adi Shankara Institute of Engineering & Technology, Kalady
    index: 2
date: 5 June 2020
bibliography: paper.bib
---

# Abstract

Private bus concession card generation can be a tedious process. Having a software to collect details of students who need the card and generate the cards to be printed in PDF or other supported formats can help speed up the process and make it more efficient. The existing software is a desktop application that runs on Windows 7. Since Microsoft stopped updates to Windows 7 OS, educational institutions need an alternative.

# Acknowledgements

Our extreme thanks to Dr. Suresh Kumar V (Principal, Adi Shankara Institute of Engineering and Technology, Kalady) for providing the necessary facilities for the completion of this project. We sincerely extend our thanks to Prof. Unnikrishnan K.N, (HOD, Computer Science and Engineering department, Adi Shankara Institute of Engineering and Technology, Kalady) for all the help, motivation and guidance throughout the completion of this project. We also wish to thank all teaching and non-teaching faculty of Computer Science and Engineering department, Adi Shankara Institute of Engineering and Technology, Kalady for their cooperation and support.

# Introduction

Our project aims at providing a cross-platform solution for printing concession cards with very minimal setup. Users can print up to 5 cards per paper. Since it's made of javascript, it can run on any modern computer operating system which can run a browser.

It has a basic authentication where the user needs to provide a password for using the software. All student details are permanently stored so that they can be reprinted. Storage is handled by a javascript library within the software and doesn't need any additional database management software. All data stored are encrypted by default.

For windows, our software is shipped as an installer which can be installed and uninstalled easily. For Linux based OS, it is shipped as a Linux package.

# Literature Survey

In order to create a solution that works on all popular operating systems, we need a framework for software development that allows compiling the source code to executables that work on all operating systems. There are many technologies available for developing cross-platform desktop apps. The most popular ones are JavaFX and Electron. Based on the [comparison done by Alkhars & Mahmoud][1] [@cross-platform], the Electron framework was chosen for this project. The Electron app gives faster execution time and less memory usage than a JavaFX app.

This project attempts to convert a JavaScript-Based Web Application to a Cross-Platform Desktop Application with Electron as defined by [Kitti Kredpattanakul and Yachai Limpiyakorn][2] [@electron].

In this project, the web application is designed using React. React is an open-source JavaScript library for building user interfaces. The React framework was chosen because it is being built with the intention of being used for building cross-platform native apps as mentioned in the [dissertation by N. Hansson and T. Vidhall][3] [@react].

# Tools/Softwares used

- **Pouch DB** - JavaScript database that stores information as JSON.
- **Electron** - Allows the building of hybrid web applications for desktop operating systems. It combines chromium GUI engine and node runtime.
- **React** - JavaScript framework for building dynamic web apps.
- **Node** - an asynchronous event-driven JavaScript runtime.
- **Git** - Version Controlling.
- **NSIS** - Nullsoft Scriptable Install System is used for building windows installers.

# Implementation

## Web Application

The project at its core is a web application. The web application is built using an open-source framework called React. In order to create a React-based web application, this project uses a common boilerplate called Create-React-App. In order to use the boilerplate, a JavaScript build toolchain is required. A JavaScript build toolchain typically consists of:

- A package manager, such as Yarn or npm. It lets developers take advantage of a vast ecosystem of third-party packages, and easily install or update them.
- A bundler, such as Webpack or Parcel. It lets developers write modular code and bundle it together into small packages to optimize load time.
- A compiler such as Babel. It lets developers write a modern JavaScript code that still works in older browsers.

The directory structure of the boilerplate looks like the following,

- README.md
- node_modules/
- package.json
- public/
  - index.html
  - favicon.ico
- src/
  - App.css
  - App.js
  - App.test.js
  - index.css
  - index.js - logo.svg

For the project to build, these files must exist with exact filenames:

- `public/index.html` is the page template;
- `src/index.js` is the JavaScript entry point.

The other files can be deleted or renamed. Subdirectories may be created inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack. Any JS and CSS files need to be inside `src`, otherwise, Webpack won't see them.

Only files inside the `public` folder can be used from `public/index.html`. Top-level directories may be created. They will not be included in the production build.

## API

The project uses ExpressJS to create an Application Programming Interface or API. The API enables the web application to communicate with the database.

Express.js, or simply Express, is a web application framework for Node.js, released as free and open-source software under the MIT License. It is designed for building web applications and APIs. It has been called the de facto standard server framework for Node.js.

The API for this project consists of the following endpoints.

- **POST /api/auth** - Used for logging in a user. Takes in user and password and returns a JSON Web Token.
- **POST /api/auth** - Used for resetting a user's password.
- **GET /api/course** - Returns a list of all courses offered by the institution.
- **POST /api/course** - Creates a new course.
- **PUT /api/course** - Modifies a course.
- **DELETE /api/course** - Deletes a course.
- **POST /api/generate** - Returns a PDF file containing concession cards of the provided list of students.
- **GET /api/student** - Returns a list of student records.
- **POST /api/student** - Creates a new student record.
- **PUT /api/student** - Modifies a student record.
- **DELETE /api/student** - Deletes a student record.

## Database

This project uses PouchDB for storing data locally on the host machine. PouchDB is a JavaScript-based database management system. It stores data in JSON documents. This is very convenient since the project uses JavaScript objects in both the API server and Front-end. PouchDB does not require any additional setup and hence can be installed easily along with the application.

# Result/Conclusion

Successfully developed a software for private bus concession card generation. It runs on any desktop based operating system that can support a browser. It has a database that doesn't need any additional installation. For Windows operating systems, this software distributed as an installer which also had an uninstaller with it. For linux based operating systems, this software is distributed as a package.

# References

[1]: http://urn.kb.se/resolve?urn=urn:nbn:se:liu:diva-130022 'A. Alkhars and W. Mahmoud, `Cross-Platform Desktop Development (JavaFX vs. Electron)`, Dissertation, 2017.'
[2]: https://doi.org/10.1007/978-981-13-1056-0_56 'Kredpattanakul K., Limpiyakorn Y. (2019) `Transforming JavaScript-Based Web Application to Cross-Platform Desktop with Electron`. In: Kim K., Baek N. (eds) Information Science and Applications 2018. ICISA 2018. Lecture Notes in Electrical Engineering, vol 514. Springer, Singapore.'
[3]: http://urn.kb.se/resolve?urn=urn:nbn:se:liu:diva-130022 'N. Hansson and T. Vidhall, `Effects on performance and usability for cross-platform application development using React Native`, Dissertation, 2016.'
