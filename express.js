if (process.versions && process.versions.electron) {
	process.env.NODE_CONFIG_DIR = require('electron').app.getAppPath() + '/config'
}
const bcrypt = require('bcryptjs')
const config = require('config')
const express = require('express')
const fs = require('fs')
const path = require('path')
const PouchDB = require('pouchdb')

const app = express()

const initializeDB = async () => {
	if (!fs.existsSync('./db')) {
		fs.mkdirSync('./db')
		let db = new PouchDB(config.get('passwordDB'))
		let info = await db.info()
		if (info.doc_count === 0) {
			const salt = await bcrypt.genSalt(10)
			password = await bcrypt.hash('password', salt)
			db.put({ _id: 'admin', password })
			console.log('Password generated')
		}

		db = new PouchDB(config.get('idDB'))
		info = await db.info()
		if (info.doc_count === 0) {
			db.put({ _id: 'cardid', no: -1 })
			console.log('Card ID initialized')
		}
	}
}

initializeDB()

app.use(express.json({ extended: false }))

app.use('/api/auth', require('./routes/auth'))
app.use('/api/course', require('./routes/course'))
app.use('/api/generate', require('./routes/card'))
app.use('/api/student', require('./routes/student'))

app.use(express.static(path.resolve(__dirname, 'build')))
app.get('*', (req, res) =>
	res.sendFile(path.resolve(__dirname, 'build', 'index.html'))
)

module.exports = app
